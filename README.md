VidyoCore iOS Framework
=========

VidyoCore is an **iOS** and **Android** frameork that adds video chat capabilities to your app.
Built on top of Vidyo APIs, this framework requires access to [VidyoWorks](https://www.vidyo.com/professional-services/developer) portal or a [vidyo.io](https://vidyo.io/) account. 

![vidyocore](vidyocore.jpg)


Features
--------

- Single point of entry: one function to start the call.
- Provides closures for call-started, call-ended and call-failed.
- Customizable (branded) in-call screen.
- CallKit support for smooth switch-over between video and an incoming phone or VoIP call. 
- Easily integrates with your own VidyoWorks portal and/or application server, for call provisioning, analytics etc.
- Built-in support for "Join as guest", test
- PiP (Picture in Picture) mode, provides a draggable video view and allows the user to continue using the app while on a video call.


Setup
------
The current release supports iOS 9.0 and above. The library is built with Swift 5. 

- Xcode
	- Language Support: Swift 5, Objective-C
	- Fully Compatible With: Xcode 12.x
 	- Minimum Supported Version: Xcode 11
- iOS
	- Fully Compatible With: iOS 14.0
	- Minimum Deployment Target: iOS 9.0

- BitCode disabled due to underlying VidyoConnector library, which does not support bitcode.
- Supported architectures: armv7, armv7s, arm64. **Does not support iOS simulator**, due to underlying VidyoClient library, which does not include i386 lib.  

### Using CocoaPods

- Add the pod vidyocore-ios to your Podfile.

```
# Private specs repo for VidyoCore pod
source 'https://gitlab.com/100grams.vidyo/vidyo-specs.git'

target 'MyApp' do
	use_frameworks!
	...
	pod 'vidyocore-ios'
end
```

- Run pod install from Terminal, then open your app's .xcworkspace file to launch Xcode.
- Import the VidyoCore package to your Swift file:
- **ADD `CoreLocation.framework` to your Xcode project** (required by VidyoClient iOS client)

```swift
import VidyoCore
```

Usage: 
-----

Join a call using a guest link: 

```swift
import VidyoCore

        VidyoViewController.create(guestLink: url, onCallEnd: { [weak self] (returnURL, error) in
                if error != 0 {
	                let alert = UIAlertController(title: nil, 
                		message: "An error has occurred while trying to join this call." + " \(error?.localizedDescription ?? "")", preferredStyle: .alert)
   	       	     	let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
   	   		        alert.addAction(okAction)
   		            self?.present(alert, animated: true, completion: nil)
                }            
                else if let url = returnURL {
                	// open the post-call returnURL in a webViewController
                   	if let webVC = self.postCallViewController(url: returnURL) {
                        self.navigationController?.pushViewController(webVC, animated: true)
                    }
                    else {
                        UIApplication.shared.openURL(url)
                        self.navigationController?.popViewController(animated: true)
                    }
                }
                else {
                    self.navigationController?.popViewController(animated: true)
                }
            
        })

```

Join a "test" call, which ends automatically after `duration`

```swift
            VidyoViewController.createTest(guestLink: guestLink, message: message, duration: duration) { (_, error) in
                if error != 0 {
                    NSLog("Test Room Error: \(error)")
                }
                self.navigationController?.popViewController(animated: true)
            }
```

Creates a new `VidyoViewController` asynchronously, after validating the `appId` and `call code`.

```swift
            VidyoViewController.createAsync(appId: kMyAppId, callCode: userEnteredCode, onCallEnd: { [weak self] (error, vidyoViewController) in
                if error != 0 {
                    NSLog("Error initializing Vidyo call: \(error)")
                }
                else if let vc - vidyoViewController {
                	self.navigationController?.pushViewController(vc, animated: true)
                }
            }
```

     

## License
<a rel="license" href="http://creativecommons.org/licenses/by-nc-nd/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-nd/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-nd/4.0/">Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License</a> and subject to <a rel="vidyo-eula" href="https://www.vidyo.com/eula">Vidyo End User License Agreement</a>.

To obtain a commercial license please contact us. 

## Maintenance 
Steps for updating this repo and publishing a new podspec version:

1. Build VidyoCore.framework from source
2. Copy the new VidyoCore.framework folder to this repo
3. Update podspec (version number)
4. Commit and push the changes to the remote git repo
5. run `git tag` and push `git push --tags` to update the remote
4. `pod spec lint --skip-import-validation vidyocore-ios.podspec`
5. `pod repo push --skip-import-validation gitlab-100grams.vidyo-vidyo-specs vidyocore-ios.podspec`


## <img src="./100grams.png" width="32" height="32"> [100grams](https://100grams.io)

- Get a free trial for a commercial license and start using this framework 
- More guidance on how to embed this framework
- Customize this framework 

[email](mailto:hello@100grams.nl?subject=VidyoCore.framework) | [twitter](https://twitter.com/100gramsapps)
