//
//  vidyocore-ios-framework.h
//  vidyocore-ios-framework
//
//  Created by Rajeev Bhatia on 9/21/17.
//  Copyright © 2017 Rajeev Bhatia. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for vidyocore-ios-framework.
FOUNDATION_EXPORT double vidyocore_ios_frameworkVersionNumber;

//! Project version string for vidyocore-ios-framework.
FOUNDATION_EXPORT const unsigned char vidyocore_ios_frameworkVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <vidyocore_ios_framework/PublicHeader.h>

#import "VidyoConnector_Objc.h"
