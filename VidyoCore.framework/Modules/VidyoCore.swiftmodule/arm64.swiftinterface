// swift-interface-format-version: 1.0
// swift-compiler-version: Apple Swift version 5.4.2 (swiftlang-1205.0.28.2 clang-1205.0.19.57)
// swift-module-flags: -target arm64-apple-ios11.0 -enable-objc-interop -enable-library-evolution -swift-version 5 -enforce-exclusivity=checked -Onone -module-name VidyoCore
import AVFoundation
import CallKit
import Foundation
import Swift
import UIKit.UIGestureRecognizerSubclass
import UIKit
@_exported import VidyoCore
public typealias CompletionBlock = (Swift.Bool, [VidyoCore.VCVidyoParameter : Any]?, Swift.Int) -> Swift.Void
public typealias ToastDetailsType = (message: Swift.String, duration: Foundation.TimeInterval)
public enum VCVidyoParameter {
  case guestlink
  case recording
  public static func == (a: VidyoCore.VCVidyoParameter, b: VidyoCore.VCVidyoParameter) -> Swift.Bool
  public func hash(into hasher: inout Swift.Hasher)
  public var hashValue: Swift.Int {
    get
  }
}
public enum VCVidyoAPI {
  case login
  case createScheduleRoom
  case joinRoom
  case endCall
  case recording
  public static func == (a: VidyoCore.VCVidyoAPI, b: VidyoCore.VCVidyoAPI) -> Swift.Bool
  public func hash(into hasher: inout Swift.Hasher)
  public var hashValue: Swift.Int {
    get
  }
}
public enum JoinAs {
  case Guest(url: VidyoCore.GuestLink)
  case Test(url: VidyoCore.GuestLink, message: Swift.String, duration: Swift.Double)
}
public enum GuestLinkFormat {
  case standard
  case vidyoNew
  case vidyoLegacy
  case newDeeplink
  case oldDeeplink
  public static func == (a: VidyoCore.GuestLinkFormat, b: VidyoCore.GuestLinkFormat) -> Swift.Bool
  public func hash(into hasher: inout Swift.Hasher)
  public var hashValue: Swift.Int {
    get
  }
}
public typealias GuestLink = Foundation.NSURL
extension NSURL {
  public var vidyoKey: Swift.String? {
    get
  }
  public var vidyoPortal: Swift.String? {
    get
  }
  public var vidyoName: Swift.String? {
    get
  }
  public var meetingID: Swift.String? {
    get
  }
  public var profileID: Swift.String? {
    get
  }
  public var onEndURL: Foundation.URL? {
    get
  }
  public var hasActiveSpeaker: Swift.Bool? {
    get
  }
}
@objc @_inheritsConvenienceInitializers @_hasMissingDesignatedInitializers open class ToastCenter : ObjectiveC.NSObject {
  open var currentToast: VidyoCore.Toast? {
    get
  }
  @objc public var isSupportAccessibility: Swift.Bool
  @objc public var isQueueEnabled: Swift.Bool
  @objc public static let `default`: VidyoCore.ToastCenter
  open func add(_ toast: VidyoCore.Toast)
  @objc open func cancelAll()
  @objc deinit
}
public struct VCUserDefaultsKey {
  public static let guestName: Swift.String
  public static let roomPin: Swift.String
}
@_hasMissingDesignatedInitializers public class VCVidyoHandler {
  public class var sharedInstance: VidyoCore.VCVidyoHandler {
    get
  }
  public func refreshUI(videoView: inout UIKit.UIView, size: CoreGraphics.CGSize)
  @objc deinit
}
extension VCVidyoHandler : VidyoCore.VCConnectorIConnect {
  @objc dynamic public func onSuccess()
  @objc dynamic public func onFailure(_ reason: VidyoCore.VCConnectorFailReason)
  @objc dynamic public func onDisconnected(_ reason: VidyoCore.VCConnectorDisconnectReason)
}
extension VCVidyoHandler : VidyoCore.VCConnectorIRegisterLocalCameraEventListener {
  public func selectCamera(atPosition pos: VidyoCore.VCLocalCameraPosition)
  @objc dynamic public func onLocalCameraAdded(_ localCamera: VidyoCore.VCLocalCamera!)
  @objc dynamic public func onLocalCameraRemoved(_ localCamera: VidyoCore.VCLocalCamera!)
  @objc dynamic public func onLocalCameraSelected(_ localCamera: VidyoCore.VCLocalCamera?)
  @objc dynamic public func onLocalCameraStateUpdated(_ localCamera: VidyoCore.VCLocalCamera!, state: VidyoCore.VCDeviceState)
}
extension VCVidyoHandler : VidyoCore.VCConnectorIRegisterLocalMicrophoneEventListener {
  @objc dynamic public func onLocalMicrophoneAdded(_ localMicrophone: VidyoCore.VCLocalMicrophone!)
  @objc dynamic public func onLocalMicrophoneRemoved(_ localMicrophone: VidyoCore.VCLocalMicrophone!)
  @objc dynamic public func onLocalMicrophoneSelected(_ localMicrophone: VidyoCore.VCLocalMicrophone?)
  @objc dynamic public func onLocalMicrophoneStateUpdated(_ localMicrophone: VidyoCore.VCLocalMicrophone?, state: VidyoCore.VCDeviceState)
}
extension VCVidyoHandler : VidyoCore.VCConnectorIRegisterLocalSpeakerEventListener {
  @objc dynamic public func onLocalSpeakerAdded(_ localSpeaker: VidyoCore.VCLocalSpeaker!)
  @objc dynamic public func onLocalSpeakerRemoved(_ localSpeaker: VidyoCore.VCLocalSpeaker!)
  @objc dynamic public func onLocalSpeakerSelected(_ localSpeaker: VidyoCore.VCLocalSpeaker?)
  @objc dynamic public func onLocalSpeakerStateUpdated(_ localSpeaker: VidyoCore.VCLocalSpeaker?, state: VidyoCore.VCDeviceState)
}
extension VCVidyoHandler : VidyoCore.VCConnectorIRegisterLogEventListener {
  @objc dynamic public func onLog(_ logRecord: VidyoCore.VCLogRecord!)
}
extension VCVidyoHandler : VidyoCore.VCConnectorIRegisterRecorderInCallEventListener {
  @objc dynamic public func recorder(inCall hasRecorder: Swift.Bool, isPaused: Swift.Bool)
}
@objc open class ToastView : UIKit.UIView {
  open var text: Swift.String? {
    get
    set
  }
  open var attributedText: Foundation.NSAttributedString? {
    get
    set
  }
  @objc override dynamic open var backgroundColor: UIKit.UIColor? {
    @objc get
    @objc set
  }
  @objc dynamic open var cornerRadius: CoreGraphics.CGFloat {
    @objc get
    @objc set
  }
  @objc dynamic open var textInsets: UIKit.UIEdgeInsets
  @objc dynamic open var textColor: UIKit.UIColor? {
    @objc get
    @objc set
  }
  @objc dynamic open var font: UIKit.UIFont? {
    @objc get
    @objc set
  }
  @objc dynamic open var bottomOffsetPortrait: CoreGraphics.CGFloat
  @objc dynamic open var bottomOffsetLandscape: CoreGraphics.CGFloat
  @objc dynamic open var useSafeAreaForBottomOffset: Swift.Bool
  @objc dynamic open var maxWidthRatio: (CoreGraphics.CGFloat)
  @objc dynamic open var shadowPath: CoreGraphics.CGPath? {
    @objc get
    @objc set
  }
  @objc dynamic open var shadowColor: UIKit.UIColor? {
    @objc get
    @objc set
  }
  @objc dynamic open var shadowOpacity: Swift.Float {
    @objc get
    @objc set
  }
  @objc dynamic open var shadowOffset: CoreGraphics.CGSize {
    @objc get
    @objc set
  }
  @objc dynamic open var shadowRadius: CoreGraphics.CGFloat {
    @objc get
    @objc set
  }
  @objc dynamic public init()
  @objc required convenience dynamic public init?(coder aDecoder: Foundation.NSCoder)
  @objc override dynamic open func layoutSubviews()
  @objc override dynamic open func hitTest(_ point: CoreGraphics.CGPoint, with event: UIKit.UIEvent!) -> UIKit.UIView?
  @objc override dynamic public init(frame: CoreGraphics.CGRect)
  @objc deinit
}
@objc open class ToastWindow : UIKit.UIWindow {
  public static let shared: VidyoCore.ToastWindow
  @objc override dynamic open var rootViewController: UIKit.UIViewController? {
    @objc get
    @objc set
  }
  @objc override dynamic open var isHidden: Swift.Bool {
    @objc get
    @objc set
  }
  public init(frame: CoreGraphics.CGRect, mainWindow: UIKit.UIWindow?)
  @objc required dynamic public init?(coder aDecoder: Foundation.NSCoder)
  @objc override dynamic open func addSubview(_ view: UIKit.UIView)
  @objc override dynamic open func becomeKey()
  @available(iOS 13.0, *)
  @objc override dynamic public init(windowScene: UIKit.UIWindowScene)
  @objc override dynamic public init(frame: CoreGraphics.CGRect)
  @objc deinit
}
@objc @_inheritsConvenienceInitializers @_hasMissingDesignatedInitializers public class Delay : ObjectiveC.NSObject {
  @objc(Short) public static let short: Swift.Double
  @objc(Long) public static let long: Swift.Double
  @objc deinit
}
@objc open class Toast : Foundation.Operation {
  @objc public var text: Swift.String? {
    @objc get
    @objc set
  }
  @objc public var attributedText: Foundation.NSAttributedString? {
    @objc get
    @objc set
  }
  @objc public var delay: Foundation.TimeInterval
  @objc public var duration: Foundation.TimeInterval
  @objc override dynamic open var isExecuting: Swift.Bool {
    @objc get
    @objc set
  }
  @objc override dynamic open var isFinished: Swift.Bool {
    @objc get
    @objc set
  }
  @objc public var view: VidyoCore.ToastView
  @objc public init(text: Swift.String?, delay: Foundation.TimeInterval = 0, duration: Foundation.TimeInterval = Delay.short)
  @objc public init(attributedText: Foundation.NSAttributedString?, delay: Foundation.TimeInterval = 0, duration: Foundation.TimeInterval = Delay.short)
  @available(*, deprecated, message: "Use 'init(text:)' instead.")
  public class func makeText(_ text: Swift.String) -> VidyoCore.Toast
  @available(*, deprecated, message: "Use 'init(text:duration:)' instead.")
  public class func makeText(_ text: Swift.String, duration: Foundation.TimeInterval) -> VidyoCore.Toast
  @available(*, deprecated, message: "Use 'init(text:delay:duration:)' instead.")
  public class func makeText(_ text: Swift.String?, delay: Foundation.TimeInterval, duration: Foundation.TimeInterval) -> VidyoCore.Toast
  @objc public func show()
  @objc override dynamic open func cancel()
  @objc override dynamic open func start()
  @objc override dynamic open func main()
  @objc override dynamic public init()
  @objc deinit
}
extension UIApplication {
  @objc override dynamic open var next: UIKit.UIResponder? {
    @objc get
  }
}
extension NSNotification.Name {
  public static let JoinedGuestLinkCall: Foundation.Notification.Name
}
@objc @_inheritsConvenienceInitializers open class VidyoViewController : UIKit.UIViewController, UIKit.UIGestureRecognizerDelegate {
  public var joinAs: VidyoCore.JoinAs
  public typealias CallEnd = (Foundation.URL?, Swift.Int) -> Swift.Void
  public var allowToggleBottomBar: Swift.Bool
  public var allowToggleTileLayout: Swift.Bool
  @objc override dynamic open func viewDidLoad()
  @objc override dynamic open func viewWillAppear(_ animated: Swift.Bool)
  @objc override dynamic open func viewWillDisappear(_ animated: Swift.Bool)
  @objc override dynamic open func viewDidAppear(_ animated: Swift.Bool)
  @objc @IBAction open func endCall(_ sender: Swift.AnyObject)
  @objc override dynamic public init(nibName nibNameOrNil: Swift.String?, bundle nibBundleOrNil: Foundation.Bundle?)
  @objc required dynamic public init?(coder: Foundation.NSCoder)
  @objc deinit
}
extension VidyoViewController {
  @objc public static func create(guestLink: VidyoCore.GuestLink, onCallEnd: VidyoCore.VidyoViewController.CallEnd?) -> VidyoCore.VidyoViewController
  @objc public static func createTest(guestLink: VidyoCore.GuestLink, message: Swift.String, duration: Swift.Double, onCallEnd: VidyoCore.VidyoViewController.CallEnd?) -> VidyoCore.VidyoViewController
  @objc public static func createAsync(appId: Swift.String, callCode: Swift.String, onCallEnd: VidyoCore.VidyoViewController.CallEnd?, completion: ((Swift.Error?, VidyoCore.VidyoViewController?) -> ())?)
}
extension VidyoViewController {
  @objc override dynamic open var supportedInterfaceOrientations: UIKit.UIInterfaceOrientationMask {
    @objc get
  }
  @objc override dynamic open var shouldAutorotate: Swift.Bool {
    @objc get
  }
  @objc override dynamic open func viewWillTransition(to size: CoreGraphics.CGSize, with coordinator: UIKit.UIViewControllerTransitionCoordinator)
}
extension VidyoViewController : UIKit.UINavigationControllerDelegate {
  @objc dynamic public func navigationControllerSupportedInterfaceOrientations(_ navigationController: UIKit.UINavigationController) -> UIKit.UIInterfaceOrientationMask
}
extension VidyoCore.VCVidyoParameter : Swift.Equatable {}
extension VidyoCore.VCVidyoParameter : Swift.Hashable {}
extension VidyoCore.VCVidyoAPI : Swift.Equatable {}
extension VidyoCore.VCVidyoAPI : Swift.Hashable {}
extension VidyoCore.GuestLinkFormat : Swift.Equatable {}
extension VidyoCore.GuestLinkFormat : Swift.Hashable {}
